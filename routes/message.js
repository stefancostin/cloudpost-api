var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.send('Sample Message.');
});

module.exports = router;
